<?php
class Category extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model("Category_model");
	}

	public  function test($data=array()){
		echo "<pre>";
		print_r($data);
		echo "</pre>";
		die;
	}

	public function index()
	{
		$data["mainCategories"] = $this->Category_model->getCategoriesByParent();
		$this->load->view('categories_view',$data);
	}

	public function getSubCategories(){
		$parent_id   = $this->input->get("parent_id");
		echo json_encode($this->Category_model->getCategoriesByParent($parent_id));
	}

}
