<?php
class Category_model extends CI_Model{

	public function __construct()
	{
		parent:: __construct();
	}
	public $_table = 'categories';

	public function getCategoriesByParent($parent_id = null){
		$this->db->select("*");
		$this->db->from($this->_table);
		$this->db->where("parent_id",$parent_id );
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
		return [];
	}
}
