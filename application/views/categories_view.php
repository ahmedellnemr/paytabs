
<html>
<head>
	<title>Codeigniter Dynamic Dependent Select Box using Ajax</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
	<style>
		.box
		{
			width:100%;
			max-width: 650px;
			margin:0 auto;
		}
	</style>
</head>
<body>
<div class="container box">
	<br />
	<br />
	<br />
	<div class="form-group">
		<select name="category" id="category" sub-id="sub_category" class="get-sub-categories  form-control input-lg">
			<option value="">Select Category</option>
			<?php if (isset($mainCategories) && !empty($mainCategories)): ?>
				<?php foreach ($mainCategories as $category): ?>
					<option value="<?=$category->id?>"><?=$category->title?></option>
				<?php endforeach ?>
			<?php endif ?>
		</select>
	</div>
	<br />
	<div class="form-group">
		<select name="sub_category" id="sub_category" sub-id="sub_sub_category" class="get-sub-categories form-control input-lg">
			<option value="">Select Main Category First</option>
		</select>
	</div>
	<br />
	<div class="form-group">
		<select name="sub_sub_category" id="sub_sub_category" class="form-control input-lg">
			<option value="">Select Main Category First</option>
		</select>
	</div>
</div>
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script>
	$('.get-sub-categories').on('change',function(){
		var selectObj =  $(this) ;
		var parent_id = selectObj.val() || 0 ;
		if(parent_id != '' && parent_id != 0 )
		{
			getSubCategory(parent_id,selectObj.attr("sub-id"));
		}
	});

	function getSubCategory(id,selectId){
		$.ajax({
			url:"<?=base_url()?>/get-sub-categories",
			method:"GET",
			data:{parent_id :id},
			dataType: 'json',
			cache: true,
			success:function(data)
			{
				$('#'+selectId).children().remove();
				if(data.length > 0 ){
					$('#'+selectId).append($("<option></option>")
							.attr("value","")
							.text("Select Category"));
					$.each(data, function(key, value) {
						$('#'+selectId).append($("<option></option>")
								.attr("value", value.id)
								.text(value.title));
					});
				}
				else{
					$('#'+selectId).append($("<option></option>")
							.attr("value","")
							.text("There are no subcategories"));
				}
			},
			error: function (error) {
				console.log(500)
				console.log(error);
			}
		});
	}


</script>

</html>
